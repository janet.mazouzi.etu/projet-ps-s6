#include <stdio.h>
#include <stdlib.h>

#include "inout.h"


void rectangle(int selection[HAUTEUR][LARGEUR]){
  int xa,xb,ya,yb;                                                                      //Rentrée des coordonnées par l'utilisateur
  printf("Veuillez entrer les coordonnées du point A, le point inférieur gauche du rectangle\n");
  printf("xa:\n");
  scanf("%d",&xa);
  printf("ya:\n");
  scanf("%d",&ya);

  printf("Veuillez entrer les coordonnées du point B, le point supérieur droit du rectangle\n");
  printf("xb:\n");
  scanf("%d",&xb);
  printf("yb:\n");
  scanf("%d",&yb);
	
  if((xa<xb)&&(ya>yb)&&(xa>=0)&&(xb<LARGEUR)&&(yb>=0)&&(ya<HAUTEUR)){         // Test de compatibilité des coordonnées 
    for (int i=yb;i<ya+1;i++){
      for(int j=xa; j<xb+1;j++){
	selection[i][j]=1;                                                    // Modification de selection 
      }
    }
    printf("Sélection réussie\n");
  }
  else printf("Coordonnées non-compatibles. Veuillez essayer à nouveau.\n");
	  
}
		


void ellipse(int selection[HAUTEUR][LARGEUR]){
  int xc,yc,a,b;                                                               // Rentrée des coordonnées par l'utilisateur
  printf("Veuillez entrer les coordonnées du point C, le centre de l'éllipse.\n");
  printf("xc:\n");
  scanf("%d",&xc);
  printf("yc:\n");
  scanf("%d",&yc);
       
  printf("Veuillez entrer les valeurs du demi grand axe a et du demi petit axe b.\n");
  printf("a:\n");
  scanf("%d",&a);
  printf("b:\n");
  scanf("%d",&b);

  if((xc>=0)&&(yc>=0)&&(xc<LARGEUR)&&(yc<HAUTEUR)&&(a>0)&&(b>0)){                // Test de compatibilité des coordonnées
    
    int x_ini,x_fin,y_ini,y_fin;                                   // Adaptation des bornes des boucles en cas de sortie d'image
    if(xc-a<0) x_ini=0;
    else x_ini=xc-a;

    if(xc+a>=LARGEUR) x_fin=LARGEUR-1;
    else x_fin=xc+a;

    if(yc-b<0)y_ini=0;
    else y_ini=yc-b;

    if(yc+b>=HAUTEUR) y_fin=HAUTEUR-1;
    else y_fin=yc+b;

    for(int i=y_ini; i<y_fin; i++){
      for (int j = x_ini; j < x_fin; j++) {
	if(( ((j-xc)*(j-xc)*1.0)/(a*a) + ((i-yc)*(i-yc)*1.0)/(b*b) )<=1.0) selection[i][j]=1 ; // Modification de selection
      }
    }
    printf("Sélection reussite\n");
  }
  else printf("Coordonnées non-compatibles. Veuillez essayer à nouveau.\n");
}

void  magicwand(int selection[HAUTEUR][LARGEUR],unsigned char image[HAUTEUR][LARGEUR]){
  int xc,yc,precision,shade;
  printf("Veuillez entrer les coordonnées du pixel choisi.\n");
  printf("xc:\n");
  scanf("%d",&xc);
  printf("yc:\n");
  scanf("%d",&yc);
  printf("Veuillez entrer la précision de la baguette\n");
  scanf("%d",&precision);
  if((xc>=0)&&(yc>=0)&&(xc<LARGEUR)&&(yc<HAUTEUR)){
    shade=image[yc][xc]; 
    selection[yc][xc]=1;
    int trouve=1;
    while(trouve==1){
      trouve=0;
      for(int i=0;i<HAUTEUR;i++){
	for(int j=0;j<LARGEUR;j++){
	  if((selection[i][j]!=1)&&(((i!=0)&&(selection[i-1][j]==1)) || ((i!=HAUTEUR-1)&&(selection[i+1][j]==1))||((j!=LARGEUR-1)&&(selection[i][j+1]==1))||((j!=0)&&(selection[i][j-1]==1)))){
	    if((image[i][j]<shade+precision)&&(image[i][j]>shade-precision)){
	      selection[i][j]=1;
	      trouve=1;
	    }
	  }
	}
      }
    }
    printf("Sélection réalisée avec succés\n");
  }
  else printf("Coordonnées non-compatibles. Veuillez essayer à nouveau.\n");
}


int main()
{
  printf("Bienvenue.\n");
  unsigned char image[HAUTEUR][LARGEUR];
  unsigned char image2[HAUTEUR][LARGEUR];
  int selection[HAUTEUR][LARGEUR];
  int charge = chargeImage("../images/royalPalaceMadrid.pgm", image);
  int charge2 = chargeImage("../images/fog-1535201_800.pgm", image2);
  if (charge == 1) {

    //Interrogation de l'utilisateur, pour qu'il choisisse la sélection à faire
    int select=4;
    while(select!=0){
      printf("Quelle sélection souhaitez-vous faire? Entrez le chiffre correspondant.\n1: Rectangle\n2: Ellipse\n3: Baguette magique\n0: Arreter la sélection\n");
      scanf("%d",&select);
    
      if(select==1){
	rectangle(selection);
      }
      if(select==2){
        ellipse(selection);
      }
      if(select==3){
	magicwand(selection,image);
	
      }
      if((select<0)||(select>3)) printf("\nLe chiffre rentré ne correspond pas aux chiffres proposés. Veuillez essayer à nouveau.\n\n"); 
    }
    
    //Modification de la sélection, ici on remplace la sélection par les pixels correspondants de l'image fog.
    if(charge2==1){
      for(int i=0; i<HAUTEUR; i++)
	for (int j = 0; j < LARGEUR; j++) {
	  if( selection[i][j]==1) image[i][j]=image2[i][j];
	}
      //sauvegarde de l'image modifiée.
      int save = sauvegardeImage("selectionmultiple.pgm", image);
      if (save == 0) printf("Chargement du fichier specifié impossible\n");
    }else printf("Chargement de la deuxième image impossible\n");
  }
  else printf("Chargement de la première image impossible\n");

  return 0;
}
