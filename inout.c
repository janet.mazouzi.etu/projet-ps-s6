/*
 *  Projet de Programmation Structurée
 *  Département IMA - 3ieme Année
 *  Polytech'Lille - 2010/2011
 *  By Jérémie Dequidt
 */

#include "inout.h"
#define DEBUG true

/*********************************************************
 * Base functions : input/output of pictures
 *********************************************************
 */


/*
 * Load a PGM picture into a char vect 
 * - Parameters : 
 *		nom_fichier: The input file
 *		vecteur: char pointer where the pixels will be stored
 * - Return value : 1 if everything went well, 0 otherwise
 */
int chargeImage(const char *nom_fichier, unsigned char vecteur[HAUTEUR][LARGEUR])
{
  /* Variable Declaration */
  FILE *fp;
  unsigned int type;
  int imax, l, h, debug;
  unsigned char buffer[TAILLE_MAX];

  /* File opening */
  if ((fp = fopen(nom_fichier, "rb")) == NULL) {
#ifdef DEBUG
    printf("[chargeImage] Impossible d'ouvrir le fichier %s\n",
           nom_fichier);
#endif
    return 0;
  }
#ifdef DEBUG
  printf("[chargeImage] Fichier %s ouvert\n", nom_fichier);
#endif
  /* 1- header */
  if (fgets((char *) buffer, TAILLE_MAX, fp) == NULL) {
#ifdef DEBUG
    printf("[chargeImage] Erreur de lecture du fichier\n");
#endif
    return 0;
  }
  if ((type = buffer[1] - 48) != 5) {
#ifdef DEBUG
    printf("[chargeImage] L'image n'est pas de type P5 (P%d)\n", type);
#endif
    return 0;
  }
  /* 2- skipping comments */
  do {
    if (fgets((char *) buffer, TAILLE_MAX, fp) == NULL) {
#ifdef DEBUG
      printf("[chargeImage] Erreur de lecture du fichier\n");
#endif
      return 0;
    }
  }
  while (buffer[0] == '#');
  /* 3- Sizes */
  if (sscanf((char *) buffer, "%d %d", &l, &h) == 0) {
#ifdef DEBUG
    printf
      ("[chargeImage] Erreur pour accéder aux dimensions de l'image\n");
#endif
    return 0;
  }
  if (fgets((char *) buffer, TAILLE_MAX, fp) == NULL) {
#ifdef DEBUG
    printf("[chargeImage] Erreur de lecture du fichier (intensite)\n");
#endif
    return 0;
  }
  if (sscanf((char *) buffer, "%d", &imax) == 0) {
#ifdef DEBUG
    printf
      ("[chargeImage] Erreur pour accéder a l'intensite max de l'image\n");
#endif
    return 0;
  }
  if ((l != LARGEUR) || (h != HAUTEUR) || (imax != INTENSITE_MAX)) {
#ifdef DEBUG
    printf
      ("[chargeImage] Erreur: les parametres de l'image sont incorrects\n");
    printf("l=%d,h=%d,imax=%d\n", l, h, imax);
    return 0;
#endif
  }
  if (vecteur == NULL) {
#ifdef DEBUG
    printf("[chargeImage] Erreur d'allocation\n");
#endif
    return 0;
  }

  /* Picture pixels recovering */
  if ((debug = fread(vecteur, 1, l * h, fp)) != (l * h)) {
#ifdef DEBUG
    printf
      ("[chargeImage] Les pixels n'ont pas été correctement récupérés = %d\n",
       debug);
#endif
    return 0;
  }

  /* Everything is ok if we arrive there */
  fclose(fp);
#ifdef DEBUG
  printf("[chargeImage] Le fichier a été correctement fermé\n");
#endif
  return 1;
}

/*
  \brief Fonction to save a picture
  \param nom_fichier output file where we want to save
  \param type output type (grey / color)
  \param tableau char array (= where to find pixels)
  \return 1 if everything went well, 0 otherwise
*/
int sauvegardeImage(const char *nom_fichier, unsigned char vecteur[HAUTEUR][LARGEUR])
{
  FILE *fp;

  /* File opening */
  if ((fp = fopen(nom_fichier, "wb")) == NULL) {
#ifdef DEBUG
    printf("[sauvegardeImage] Impossible d'ouvrir le fichier %s\n",
           nom_fichier);
#endif
    return 0;
  }
#ifdef DEBUG
  printf("[sauvegardeImage] Fichier %s ouvert en ecriture\n",
         nom_fichier);
#endif

  /* Header writing */
  if (!fprintf
      (fp, "P5\n# %s\n%d %d %d\n",
       "Polytech'Lille IMA3 - projet 2010/11", LARGEUR, HAUTEUR,
       INTENSITE_MAX)) {
#ifdef DEBUG
    printf
      ("[sauvegardeImage] Erreur pour écrire les paramètres de l'image\n");
#endif
    return 0;
  }
#ifdef DEBUG
  printf
    ("[sauvegardeImage] Entete = Image %d x %d pixels, intensite maximale = %d\n",
     LARGEUR, HAUTEUR, INTENSITE_MAX);
#endif

  /* output in file */
  if (fwrite(vecteur, 1, LARGEUR * HAUTEUR, fp) != (LARGEUR * HAUTEUR)) {
#ifdef DEBUG
    printf
      ("[sauvegardeImage] Les pixels n'ont pas été correctement écrits\n");
#endif
    return 0;
  }
  /* end of function */
  fclose(fp);
#ifdef DEBUG
  printf("[sauvegardeImage] Le fichier a été correctement fermé\n");
#endif
  return 1;
}
