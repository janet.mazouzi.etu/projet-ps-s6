CC=clang -Wall
TARGET=mainfile

all: ${TARGET}

${TARGET}: ${TARGET}.o inout.o

inout.o: inout.c

clean:
	rm -f *.o *~ ${TARGET}
